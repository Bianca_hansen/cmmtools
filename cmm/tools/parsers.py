import numpy as np

def read_chargefile(fname, string):
    '''' Reads charges containing "string" from
         a file made from grepping a bunch of gaussian
         outputs like so:

        grep -A 41 'ESP charges:' *log > charges.dat
        for a 39 atom molecule. Its important that 
        the last line for each charge set is:
        Sum of ESP charges = 

     '''

    with open(fname, 'r') as f:
        lines = f.readlines()

    out = []
    for l, line in enumerate(lines):
        if string in line:
            out.append(line.split(string)[-1])

    out = out[2:-1]

    chrgs = np.array([[int(y[0]), float(y[2])]
                       for y in [x.split()[-3:]
                       for x in out]])

    elmnts = [x.split()[2] for x in out]

    return chrgs, elmnts

def read_orca_chargefile(chgfile):
    ''' Reads charges piped to a file from the results of
        orca_chelpg.
        Works with ORCA version 4.2.0, but is very crude,
        Be careful.
    '''
    with open(chgfile, 'r') as f:
        lines = f.readlines()

    elements = []
    charges = []
    for line in lines[23:]:
        if '--------------------------------' in line:
            break
        charges.append(float(line.split()[-1]))
        elements.append(line.split()[1])

    return charges, elements
