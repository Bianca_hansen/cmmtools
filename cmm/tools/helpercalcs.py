import numpy as np
from ase.calculators.calculator import Calculator, all_changes
from ase import units as ase_u

# Dummycalc so we can run dynamics only from Hookean.
class ZeroCalc(Calculator):
    ''' Provide Zero forces and energy back '''

    implemented_properties = ['energy', 'forces']

    def __init__(self):
        Calculator.__init__(self)

    def calculate(self, atoms=None,
                  properties=['energy', 'forces'],
                  system_changes=all_changes):
        Calculator.calculate(self, atoms, properties, system_changes)

        e = 0
        f = np.zeros((len(atoms), 3))

        self.results['energy'] = e
        self.results['forces'] = f


class SimpleBond(Calculator):
    ''' Diatomic Spring '''

    implemented_properties = ['energy', 'forces']

    def __init__(self, indices, k, r0):
        Calculator.__init__(self)
        self.indices = indices
        self.k = k
        self.r0 = r0

    def calculate(self, atoms=None,
                  properties=['energy', 'forces'],
                  system_changes=all_changes):
        Calculator.calculate(self, atoms, properties, system_changes)

        e = 0
        f = np.zeros((len(atoms), 3))
        pos = atoms.get_positions()

        for i, idx in enumerate(self.indices):
            k = self.k[i]
            X = pos[idx[0]] - pos[idx[1]]
            x = np.sum(X**2)**(0.5)
            x_hat = X / x
            x -= self.r0[i]
            e += 0.5 * k * x**2
            f[idx[0]] += -k * x * x_hat
            f[idx[1]] -= -k * x * x_hat

        self.results['energy'] = e
        self.results['forces'] = f


class DoubleCalc(Calculator):
    ''' Add the results of two calculators.
        To add SimpleBond to DebyeCalc. '''

    implemented_properties = ['energy', 'forces']

    def __init__(self, calc1, calc2):
        Calculator.__init__(self)
        self.calcs = [calc1, calc2]

    def calculate(self, atoms=None,
                  properties=['energy', 'forces'],
                  system_changes=all_changes):
        Calculator.calculate(self, atoms, properties, system_changes)

        e = 0
        f = np.zeros((len(atoms), 3))

        dummy = atoms.copy()

        for calc in self.calcs:
            dummy.calc = calc
            e += dummy.get_potential_energy()
            f += dummy.get_forces()

        self.results['energy'] = e
        self.results['forces'] = f


class DummyCalc(Calculator):
    ''' Simply carries charges for CombineMM '''
    implemented_properties = ['energy', 'forces']

    def __init__(self, charges):
        self.charges = charges
        self.sites_per_mol = len(charges)
        Calculator.__init__(self)

    def get_virtual_charges(self, atoms):
        return self.charges

    def add_virtual_sites(self, positions):
        return positions

    def redistribute_forces(self, forces):
        return forces

    def calculate(self, atoms, properties, system_changes):
        Calculator.calculate(self, atoms, properties, system_changes)

        energy = 0.0
        forces = np.zeros_like(atoms.get_positions())

        self.results['energy'] = energy
        self.results['forces'] = forces


