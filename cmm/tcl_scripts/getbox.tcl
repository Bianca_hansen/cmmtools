package require pbctools 2.5;


proc dirlist { ext } {
    set contents [glob -type d $ext]
    foreach item $contents {
       append out $item
       append out "\n"
       }
    return $out
    }

proc flist { dir ext } {
    set contents [glob -directory $dir $ext]
    foreach item $contents {
       append out $item
       append out "\n"
       }
    return $out
    }


# calculates possible smallest box without any box being there in the 
# first place.. not what we need
proc get_cell {{molid top}} {
 set all [atomselect $molid all]
 set minmax [measure minmax $all]
 set vec [vecsub [lindex $minmax 1] [lindex $minmax 0]]
 set x [lindex $vec 0] 
 set y [lindex $vec 1] 
 set z [lindex $vec 2] 

 puts "cellBasisVector1 [lindex $vec 0] 0 0"
 puts "cellBasisVector2 0 [lindex $vec 1] 0"
 puts "cellBasisVector3 0 0 [lindex $vec 2]"
 set center [measure center $all]
 puts "cellOrigin $center"
 $all delete
 
 return $x $y $z

}


set dirs [dirlist *pushaway*]

foreach dir $dirs {
    puts $dir
    set dcds [flist $dir *nvt*.dcd]
    set dcd [lindex $dcds 0];  # there should only be one file per dir

    mol load parm7 agptpop_solv_custom.prmtop dcd $dcd

    set out [open $dir.cell w]
    set box [pbc get -now]
    puts $out [format "%s" $box]

    close $out

    #get_cell
}
