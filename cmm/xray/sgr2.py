import os, re, copy
import numpy as np
from .debye import Debye

class RDF:
    ''' RDF Object.

        r: np.array
            r vector
        g: np.array, same length as r
            g(r) values
        name1: str
            Name of atomtype/element of 'left' atoms
        name2: str
            Name of atomtype/element of 'right' atoms
        region1: str, 'solute' or 'solvent'
            Which region does the 'left' atom belong to
        region2: str, 'solute', or 'solvent'
            Which region does the 'right' atom belong to.

    '''

    def __init__(self, r, g, name1, name2,
                             region1, region2,
                             n1=None, n2=None,
                             damp=None, volume=None,
                             r_max=None, r_avg=None):
        self.r = r
        self.g = g
        self.name1 = name1
        self.name2 = name2
        self.region1 = region1
        self.region2 = region2

        self.n1 = n1  # number of 'left' atoms
        self.n2 = n2  # number of 'right' atoms

        self.diagonal = (name1 == name2) & (region1 == region2)
        self.cross = (region1 != region2)

        self.damp = damp  # Damping object

        self.volume = volume  # Volume used for the N/V RDF normalization
                              # (Volume of your sim. box)

        self.r_max = r_max  # Stop integrating here
        self.r_avg = r_avg  # Use avg. g(r > r_avg) for g0

        self.s = None  # Structure factor. Calculated with SGr. WIP: Move to RDF simply?


class RDFSet(dict):
    ''' Container object for a set of RDFs.
        Basically a Dict with a few more options '''

    def __init__(self, *arg, **kwargs):
        self.rdfs = {}
        super(RDFSet, self).__init__(*arg, **kwargs)

    def add_flipped(self, rdf):
        r_rdf = copy.copy(rdf)
        r_rdf.name2 = rdf.name1
        r_rdf.name1 = rdf.name2
        r_rdf.region2 = rdf.region1
        r_rdf.region1 = rdf.region2
        self.rdfs[(r_rdf.name1, r_rdf.region1,
                   r_rdf.name2, r_rdf.region2)] = r_rdf

    def __len__(self):
        return len(self.keys())


class SGr:
    ''' X-Ray Scattering from pairwise radial distribution functions.

        Can divide signal up in Solute-Solute (u-u), Solute-Solvent (u-v)
        and Solvent-Solvent (v-v) contributions.

        See e.g.: DOI: 10.1088/0953-4075/48/24/244010

    '''

    def __init__(self, volume, qvec=np.arange(0, 10, 0.01)):
        self.qvec = qvec
        self.v = volume

    def structure_factor(self, rdf):
        ''' Calculate the structure factor for a given RDF '''
        r = rdf.r
        g = rdf.g
        dr = r[1] - r[0]

        avg = 1
        if rdf.r_avg is not None:
            avg = np.mean(g[r > rdf.r_avg])

        if rdf.r_max is not None:
            mask = np.zeros(len(r), bool)
            mask[r < rdf.r_max] = True
            r = r[mask]
            g = g[mask]

        qr = self.qvec[:, None] * r[None, :]
        r2sin = 4 * np.pi * dr * (np.sinc(qr / np.pi)) * r[None, :]**2

        if (rdf.region1.lower() == 'solute') and (rdf.region2.lower() == 'solute'):
            g0 = 0
        else:
            g0 = 1  

        h = g - g0 * avg
        if rdf.damp is not None:
            h *= rdf.damp.damp(r)

        kd = int((rdf.region1 == rdf.region2) and (rdf.name1 == rdf.name2))

        ### XXX WIP: Change to partial S(Q)
        s = 1 + ((rdf.n2 - kd) / self.v) * (r2sin @ h)

        # update the rdf inplace with structure factor
        rdf.s = s

        return s

    def scoh(self, rdf):
        ''' Coherent X-Ray Scattering'''
        kdr = int((rdf.region1 == rdf.region2))
        kd = int((rdf.region1 == rdf.region2) and (rdf.name1 == rdf.name2))
        s = self.structure_factor(rdf)

        # Get form factors
        self.atomic_f0(rdf)

        term_1 = (rdf.n1 * rdf.f0_1 * rdf.f0_2 * kd) * kdr  # no first term for cross terms
        term_2 = (s - 1) * rdf.n1 * rdf.f0_1 * rdf.f0_2

        self.term_1 = term_1
        self.term_2 = term_2

        s_coh = term_1 + term_2

        return s_coh

    def atomic_f0(self, rdf):
        ''' Calculate atomic form factors and
            update RDF object in place '''
        db = Debye(self.qvec)
        rdf.f0_1 = db.atomic_f0(rdf.name1)
        rdf.f0_2 = db.atomic_f0(rdf.name2)



## Helper functions
def load_rdfs(rdflist, stoich=None, damp=None):
    ''' Make a list of RDF objects from a list of dat files from e.g. VMD

        The list should contain paths to files in the following format:

            gX_a-Y_b.dat

        where X,Y is the atom type, and a,b is either u or v, for solUte
        and solVent, respectively.

        and the .dat files should contain (r, gr) columns.

        If a stoichometry dict is provided, the n1 and n2
        of the rdf objects will also be filled.

    '''

    rdfs = []
    trslt = {'c': 'cross', 'u':'solute', 'v':'solvent'}

    for rdffile in rdflist:
        name = rdffile.split(os.sep)[-1]
        # use reg exp since atom types can be 1 or 2 chars long:
        atyp1 = re.search('g(.*?)_', name).group(1)
        part1 = re.search(atyp1 + '_(.*?)-', name).group(1)

        atyp2 = re.search('-(.*?)_', name).group(1)
        part2 = re.search('-' +  atyp2 + '_(.*).dat', name).group(1)

        dat = np.genfromtxt(rdffile)
        rdf = RDF(dat[:, 0], dat[:, 1],
                  atyp1, atyp2,
                  trslt[part1], trslt[part2], damp=damp)

        if stoich:
            rdf.n1 = stoich[(atyp1, trslt[part1])]
            rdf.n2 = stoich[(atyp2, trslt[part2])]

        rdfs.append(rdf)

    return rdfs

def rdfset_from_dir(dirname, prmtop):
    import MDAnalysis as mda
    import glob

    rdf_files = sorted(glob.glob(dirname + os.sep + '*dat'))

    whered = {'u':'solute', 'v':'solvent', 'c':'cross'}
    # Load universe to get stoichometry.
    u = mda.Universe(prmtop)

    # Unfortunately mda cannot get the box size from the prmtop, even though it is there
    with open(prmtop, 'r') as f:
        lines = f.readlines()
    for l, line in enumerate(lines):
        if 'FLAG BOX_DIMENSIONS' in line:
            box = lines[l + 2]
    box = [float(x) for x in box.split()[1:] ]
    V = np.prod(box)

    set_uc = RDFSet()  # Uncorrected

    for f, file in enumerate(rdf_files):
        data = np.genfromtxt(file)
        r = data[:, 0]
        g = data[:, 1]

        # find elements from file name. This is very specific to this exact naming scheme...
        el1 = re.search('g(.*?)_', file).group(1)
        part1 = re.search(el1 + '_(.*?)-', file).group(1)
        where1 = whered[part1]

        el2 = re.search('-(.*?)_', file).group(1)
        part2 = re.search('-' +  el2 + '_(.*).dat', file).group(1)
        where2 = whered[part2]

        Ni = len([atom for atom in u.atoms if el1 in atom.name])
        Nj = len([atom for atom in u.atoms if el2 in atom.name])

        rdf = RDF(r, g, el1, el2, where1, where2, n1=Ni, n2=Nj)
        rdf.volume = V
        set_uc[(rdf.name1, rdf.region1, rdf.name2, rdf.region2)] = rdf

    return set_uc

