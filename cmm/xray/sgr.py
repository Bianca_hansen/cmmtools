import glob, os, re, itertools
import numpy as np
from ase import Atoms
from scipy.integrate import cumtrapz, trapz
from .debye import Debye  # for atomic_f0. 
from .damping import Damping

def find_nearest(array, value=0):
    array = np.asarray(array)
    idx = (np.abs(array - value)).argmin()
    return idx, array[idx]


class SGr:
    ''' X-Ray Scattering from pairwise radial distribution functions.

        Can divide signal up in Solute-Solute (u-u), Solute-Solvent (u-v)
        and Solvent-Solvent (v-v) contributions.

        See e.g.: DOI: 10.1088/0953-4075/48/24/244010


        The rdf-loader takes in .dat files in the following format:

            gX_a-Y_b.dat

        where X,Y is the atom type, and a,b is either u or v, for solUte
        and solVent, respectively.

        Parameters:
            volume: Float
                volume of the simulation cell
            damp: str
                either any of the 'style'-inputs for the Damping class
                (see ./damping.py)
            ignore_elements: list of str
                elements to ignore when looking for rdf files. E.g.
                to avoid including scattering from counterions even
                though they are still present in the .xyz file from
                which the stoichometry is calculated.

        Uses ASE to read xyz files for the stoichometry. Kinda overkill,
        if you for some reason don't want to install the package, use your
        own xyz reader, or simply pass in a dict {'Element': Number, ...}
        to the set_stoichometry() method.

        Example:
        >>> dir_with_rdfs = '/some/location/'
        >>> sgr = SGr(volume=50**3, damp=25, ignore_elements=['Cl'] )
        >>> idx = 61  # if you have 61 atoms in your solute
        >>> sgr.set_stoichometry('single_frame_of_cell.xyz', u_idx=idx)
        >>> output = sgr.calculate(dir_with_rdfs)
        '''

    def __init__(self, volume, qvec=np.arange(0.0001, 10, 0.01), damp=None,
                 ignore_elements=[], delta=False, verbose=True, skip_missing=False):
        self.qvec = qvec
        self.v = volume
        self.ie = ignore_elements  # e.g. counterions
        self.f0 = None
        self.stoich = None
        self.all_names = None  # rdf generated from stoich for book-keeping
        self.out = None
        self.delta = delta  # True: eqn 22 is used, so dRDFs and no first term
        self.verbose = verbose
        self.skip_missing = skip_missing

        self.db = Debye(qvec)

        self.damp = damp

    def calculate(self, arg, stoich=None, ptablef0=False, bookkeep=True):
        ''' Load rdfs from files
            Figure out which atom types are in the system
            Figure out to which contribution they belong
            Get number of atom types either manually or from some other source
            Get CM parameters for these atom types
            Update an atomic form factor library
            Calculate first term
            Calculate second term.
            Format the output and output '''

        if isinstance(arg, str):  # assume its path to dir
            self.load_rdfs_fromdir(arg)
        else:
            self.rdfs = arg

        if stoich is not None:
            self.set_stoichometry(stoich)

        if ptablef0:
            self.atomic_f0_pt(self.stoich)
        else:
            self.atomic_f0(self.stoich)
            
        if bookkeep:
            self.bookkeep()

        s_u = np.zeros(len(self.qvec))
        s_v = np.zeros(len(self.qvec))
        s_t = np.zeros(len(self.qvec))

        #if not self.delta:
        s_u, s_v, s_t = self.first_term()
        if self.delta:  # first dv terms are still needed in deltas
            s_u *= 0
            s_v *= 0
            s_t *= 0

        out = self.second_term(s_u, s_v, s_t)

        return out

    def load_rdfs_fromdir(self, directory, selection=None):
        files = sorted(glob.glob(os.path.join(directory, 'g*.dat')))
        if selection is not None:
            files = sorted(glob.glob(os.path.join(directory, selection)))

        rdfs = []
        for f in files:
            name = f.split(os.sep)[-1]  # strip the directory path away

            # use reg exp since atom types can be 1 or 2 chars long:
            atyp1 = re.search('g(.*?)_', name).group(1)
            part1 = re.search(atyp1 + '_(.*?)-', name).group(1)

            atyp2 = re.search('-(.*?)_', name).group(1)
            part2 = re.search('-' +  atyp2 + '_(.*).dat', name).group(1)

            if (atyp1 in self.ie) | (atyp2 in self.ie):
                continue

            data = np.genfromtxt(f)

            cn_not_normed = cumtrapz(data[:, 0]**2 * data[:, 1], dx=data[1, 0] - data[0, 0])
            # If data files do not contain cns:
            if data.shape[1] <= 2:
                cn = np.zeros_like(cn_not_normed)
            else:
                cn = data[:, 2]

            rdf = {'name': name.strip('.dat'),
                   'type1':atyp1, 'part1':part1,
                   'type2':atyp2, 'part2':part2,
                   'r':data[:, 0], 'gr':data[:, 1],
                   'cn_vmd':cn, 'cn':cn_not_normed}
            rdfs.append(rdf)

        self.rdfs = rdfs

    def set_stoichometry(self, stoich, u_idx=None):
        ''' If the input is a dict, it assumes that the dict contains:
            {Atomtype: Number}. if not, it assumes that the input
            is an ASE-readable structure file from which it can
            figure out the stoichometry itself. '''

        if isinstance(stoich, dict):  # already formatted
            self.stoich = stoich
        elif isinstance(stoich, str):  # from xyz path
            from ase.io import read
            atoms = read(stoich)
            solu_atoms = atoms[:u_idx]
            solv_atoms = atoms[u_idx:]

            syms = solu_atoms.get_chemical_symbols()
            u_syms = [sym + '_u' for sym in syms]

            syms = solv_atoms.get_chemical_symbols()
            v_syms = [sym + '_v' for sym in syms]

            syms = u_syms + v_syms

            unique, counts = np.unique(syms, return_counts=True)
            stoich = dict(zip(unique, counts))

            # remove ignored atomtypes (yes, this is a bit ugly)
            for where in ('_u', '_v'):
                for sym in self.ie:
                    stoich.pop(sym + where, None)

            self.stoich = stoich
        elif (stoich is None) and (isinstance(self.stoich, dict)):  # already set
            if self.verbose:
                print('Stoichomestry already set')
            pass
        else:
            raise RuntimeError(f'Couldnt set stoichometry')

    def atomic_f0(self, stoich=None):
        if stoich is None:
            stoich = self.stoich
        f0 = {}  # lol how do i dict comprehend
        for sym in stoich.keys():
            only_sym = sym.split('_')[0]
            f0[only_sym] = self.db.atomic_f0(only_sym)

        self.f0 = f0

    def atomic_f0_pt(self, stoich=None):
        from periodictable.cromermann import fxrayatq
        if stoich is None:
            stoich = self.stoich
        f0 = {}
        for sym in stoich.keys():
            only_sym = sym.split('_')[0]
            f0[only_sym] = np.array([fxrayatq(only_sym, q) for q in self.qvec])
        self.f0 = f0

    def bookkeep(self):
        ''' Check if all combinations of atomtypes in stoich are in rdfs '''
        all_names = [f'{x}-{y}' for x in self.stoich.keys() for y in self.stoich.keys()]
        for key, val in self.stoich.items():
            if val == 1:  # no key-key RDF then
                all_names.remove(key + '-' + key)

        rdf_names = [rdf['type1'] + '_' + rdf['part1'] + '-' +
                     rdf['type2'] + '_' + rdf['part2'] for rdf in self.rdfs]

        missing = [name for name in all_names if name not in rdf_names]
        if self.verbose:
            print(f'Bookkeeping messages:')
            print(f'---------------------')
            for m in missing:
                print(f'NB! Missing RDF file for the pair: {m}')

            print(f'---------------------\n')

        self.all_names = all_names

    def get_rdf(self, name):
        return [rdf for rdf in self.rdfs if name in rdf['name']][0]

    def first_term(self):
        s_u = np.zeros(len(self.qvec))
        s_v = np.zeros(len(self.qvec))
        s_t = np.zeros(len(self.qvec))

        for atype, count in self.stoich.items():
            if '_u' in atype:
                f0 = self.f0[atype.strip('_u')]
                s_u += count * f0**2

            elif '_v' in atype:
                f0 = self.f0[atype.strip('_v')]
                s_v += count * f0**2
            s_t += count * f0**2


        self.debug_s_vfirst = np.copy(s_v)
        self.debug_s_ufirst = np.copy(s_u)

        return s_u, s_v, s_t

    def second_term(self, s_u, s_v, s_t):
        s_c = np.zeros(len(self.qvec))
        s_c_damp = np.zeros(len(self.qvec))
        s_u_damp = np.copy(s_u)
        s_u_2nd = np.zeros(len(self.qvec))
        s_v_damp = np.copy(s_v)
        s_t_damp = np.copy(s_t)

        fourpi = np.pi * 4
        stoich = self.stoich
        f0 = self.f0

        if not self.damp:  # semi-safe standard damping settings if nothing set:
            damp = Damping('simple', self.rdfs[0]['r'] / 2)

        first = True
        if self.verbose:
            print(f'Second term:')
            print(f'------------')
        for sn, stoich_name in enumerate(self.all_names):  # only loop over necessary rdfs
            extra_str = ''
            # find corresponding rdf:
            rdf_list = [rdf for rdf in self.rdfs if stoich_name in rdf['name']]
            if len(rdf_list) > 2:
                raise RuntimeError('More than two RDF for the same atomtype pair was found!')
            if (len(rdf_list) == 2) and self.verbose:
                print('Warning: 2 RDFs found: '+rdf_list[0]['name'] + ' and ' + rdf_list[1]['name']
                       + f' for {stoich_name}')
            if len(rdf_list) == 0:  # didn't find it, try to reverse the name
                rev_name = stoich_name.split('-')[1] + '-' +  stoich_name.split('-')[0]
                rdf_list = [rdf for rdf in self.rdfs if rev_name in rdf['name']]

                extra_str = ''
                if self.verbose and (rev_name != stoich_name):
                    extra_str = f'Using {rev_name} for {stoich_name}'
                if len(rdf_list) == 0:
                    if not self.skip_missing:
                        raise RuntimeError(f'Could not find RDF for {stoich_name}')
                    else:
                        print(f'Could not find RDF for {stoich_name}, skipping...')
                        continue
            rdf = rdf_list[0]

            if first:
                first = False
                locator = np.zeros(len(self.all_names))

            atm1 = rdf['type1'] + '_' + rdf['part1']
            atm2 = rdf['type2'] + '_' + rdf['part2']
            n_i = stoich[atm1]
            n_j = stoich[atm2]

            R = rdf['r']
            g = rdf['gr']

            damp = self.damp

            dr = R[1] - R[0]

            h = np.zeros_like(self.qvec)
            h_damp = np.zeros_like(self.qvec)

            # Find out which term this RDF belongs to
            if (rdf['part1'] == 'u') & (rdf['part2'] == 'u'):
                where = 'SOLUTE'
                g_end = 0
            elif (rdf['part1'] == 'v') & (rdf['part2'] == 'v'):
                where = 'SOLVENT'
                g_end = 1
            elif ((rdf['part1'] == 'v') & (rdf['part2'] == 'u') |
                  (rdf['part1'] == 'u') & (rdf['part2'] == 'v')):
                where = 'CROSS'
                g_end = 1
            else:
                raise RuntimeError('An RDF doesnt belong to any of the 3 regions')
            if self.delta:
                g_end = 0

            # Kronecker-delta.
            kd = float((rdf['type1'] == rdf['type2'])
                       and where != 'CROSS')

            for q, qq in enumerate(self.qvec):
                f_i = f0[rdf['type1']][q]
                f_j = f0[rdf['type2']][q]

                this_h = (g - g_end) * (np.sinc(qq * R / np.pi)) * fourpi * R**2 #/ (qq * R)
                h[q] += trapz(this_h, dx=dr)
                if damp is not None:
                    h_damp[q] += trapz(damp.damp(R) * this_h, dx=dr)

                if where == 'SOLUTE':
                    locator[sn] = 0
                    prefac = n_i * (n_j - kd) * f_i * f_j / self.v
                    s_u[q] += prefac * h[q]
                    s_u_2nd[q] += prefac * h[q]
                    s_u_damp[q] += prefac * h_damp[q]


                elif where == 'SOLVENT':
                    locator[sn] = 1
                    prefac = n_i * (n_j - kd) * f_i * f_j / self.v
                    s_v[q] += prefac * h[q]
                    s_v_damp[q] += prefac * h_damp[q]
                elif where == 'CROSS':
                    locator[sn] = 2
                    prefac = n_i * n_j * f_i * f_j / self.v
                    s_c[q] += prefac * h[q]
                    s_c_damp[q] += prefac * h_damp[q]
                    
                else:
                    raise RuntimeError('An RDF doesnt belong to any of the 3 regions')
                # Total:
                prefac = n_i * (n_j - kd) * f_i * f_j / self.v
                s_t[q] += prefac * h[q]
                s_t_damp[q] += prefac * h_damp[q]
            if self.verbose:
                if where == 'SOLUTE':
                    print(f"{where:8s}: {atm1:4s}:{n_i:7g} - {atm2:4s}:{n_j:7g}, KD:{kd}" + extra_str)
                else:
                    print(f"{where:8s}: {atm1:4s}:{n_i:7g} - {atm2:4s}:{n_j:7g}, KD:{kd} " + extra_str)
        self.s_u_2nd = s_u_2nd
        out = {'rdf':self.rdfs,
               'q': self.qvec,
               's_u': s_u,
               's_u_damp': s_u_damp,
               's_v': s_v,
               's_v_damp': s_v_damp,
               's_c': s_c,
               's_c_damp': s_c_damp,
               's_t': s_t,
               's_t_damp': s_t_damp,
               'locator': locator}
        self.out = out

        return out
