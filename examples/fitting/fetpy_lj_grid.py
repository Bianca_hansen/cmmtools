import os
import numpy as np
from mpi4py import MPI
from ase.io import Trajectory
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from cmm.fitting.get_energy import get_energy
from cmm.fitting.lj_fit import LJFit, standardize_f
from cmm.fitting.parameters import lj_parms_fet, lj_FeTH2O

''' Grid Scan of Fe x0 guesses, parallelized through MPI 

    The epsilon grid is distributed across CPUs, and 
    the script should be run with:
        mpiexec -N 10 python -u fetpy_lj_grid.py

    (and you should have 10 cores... If not, fit the grid
    spanning to the amount you have.)

    Requires a trajectory of the structures for calculating
    MM binding energies from, partial charges on the molecule, 
    and the target binding energies.

    Target file:
        N Columns: Step#, Etotal, Emono1, Emono2, ... 

'''
mol_size = 59

comm = MPI.COMM_WORLD
rank = comm.Get_rank()

traj_file = '/home/asod/Dropbox/DTU2/FeTERPY/dissimilar_dimers_over1.0rmsd_nopbc.traj'
traj = Trajectory(traj_file, 'r')
charges = np.genfromtxt('/home/asod/Dropbox/DTU2/RuBPYSCME/WithMatyas/opt_geoms/TPY/Fe-terpy-LS-CHELPG.txt')[:, 2]

# Mask: True is the non-solvents
mask = np.zeros(len(traj[0]), bool)
mask[:mol_size] = True
# Get the coulom energy
energy = np.zeros((len(traj), 3))
for a, atoms in enumerate(traj):
    atoms.pbc = False
    energy[a] = get_energy(atoms, mask, charges, mol_size=mol_size, lj=lj_parms_fet)


# Define fitting
niter = 100  # Number of basin iters
temp = 100   # Basinhopping temperature
interval=10  # Hop interval
lam = 0.01   # Regularization value  (Low, because we don't care so much about original OPLS) 
step = 0.1   # Basinhopping start step size

# Define indices of atoms belonging to the various monomers.
# Mono1 HAS to be the QM, and the rest should be MM: 
mono_idxs = [list(range(mol_size)),                  # QM
             [mol_size, mol_size + 1, mol_size +2 ]] # MM1

# Init fit object:
f = LJFit(mono_idxs, traj_file, 
          elements=['Fe', 'N', 'C', 'H'], comm=comm)

# Create target to fit to - here it is the QMQM binding energy + the D3 binding energy:
f.load_target('/home/asod/Dropbox/DTU2/FeTERPY/MultimerFits/Dissimilar_Dimers/QMQM_binding_gs_full.dat')

# load a list of atoms objects from the steps# in the trajectory:
f.load_atoms()

# Set coulomb part, using get_energy from earlier
f.set_mm_coulomb(energy[:, 1]) 

# Set lambda
f.lam = lam

# weigh such that the most binding and most repulsive values have strongest weights
#f.weights = 1000 * f.target**2

# bounds
f.bounds = [(0.0005,  0.50),   # Fe
            (0.0003,  0.10),   # N
            (0.0004,  0.04),   # C
            (0.000002, 0.03),   # H
            # Sigma
            (0.25, 8),   # Fe            
            (1, 5),      # N
            (1, 5),      # C
            (0.005, 5)]  # H

# span grid for Fe x0s
grid = np.linspace(f.bounds[0][0], f.bounds[0][1], 10)
grid_sig = np.linspace(f.bounds[4][0], f.bounds[4][1], 10)

for g_sig in grid_sig:
    x0_dict = lj_parms_fet.copy()
    x0_dict['C'] = (0.02, 3.43020)
    x0_dict['Fe'] = (grid[rank], g_sig)
    x0_dict['H'] =  (0.002 * 0.01, 2.571 * 0.01)
    x0 = []
    for el in f.elements:
        x0.append(x0_dict[el][0])
    for el in f.elements:
        x0.append(x0_dict[el][1])
    x0 = tuple(x0)

    # weigh such that the most binding and most repulsive values have strongest weights
    f.weights = 100 * standardize_f(np.abs(f.target), min(np.abs((f.target))), max(np.abs(f.target)))**2 + 1

    oname = f'fetpy_ljfit_grid_lam{f.lam:g}_niter{niter}_intvl{interval}_step{step}_T{temp}_x0Ireps{grid[rank]:06.5f}sig{g_sig:06.5f}' 
    print(f'RANK {rank}: {oname}')
    if os.path.exists(oname + '.npy'):
        print(f'Rank {rank}, {oname}.npy exists, skipping...')
        continue

    opt = f.basin_fit(x0, niter=niter, interval=interval, stepsize=step, T=temp, loss=f.regu_sl1_loss)

    np.save(oname + '.npy', opt)

    ### Save pdf of results
    lj = f.make_lj_object(opt.x)

    e_new = f.coul + f.calculate_lj(lj)
    e_old = f.coul + f.calculate_lj(lj_FeTH2O)

    fig = plt.figure(constrained_layout=True, figsize=(10, 10))
    spec = gridspec.GridSpec(ncols=1, nrows=3, figure=fig)
    ax0 = fig.add_subplot(spec[:2, 0])
    ax1 = fig.add_subplot(spec[2, 0])
    axes = [ax0, ax1]

    f.print_results(f.populate_dict(opt.x), lj_parms_fet, ax=axes[1])
    axes[1].axis('off')

    res_txt = f'Old Residual: {sum(abs(f.target - e_old)):g} '+\
              f'New Residual: {sum(abs(f.target - e_new)):g}'

    print(f'Rank {rank}: ' + res_txt)    
    axes[1].text(0, 0.3, res_txt)

    ax = axes[0]
    ax.plot(f.target, e_new, 'x', label='Fitted Parameters')
    ax.plot(f.target, e_old, 'o', label='OPLS')
    ax.plot(sorted(f.target), sorted(f.target), '--', color='r', label='perfect')
    ax.set_xlabel('QMD/QMD3 Interaction Energy (eV)')
    ax.set_ylabel('New MM/MM Interaction Energy (eV)')
    xl = ax.get_xlim()
    yl = ax.get_ylim()
    ax.text(xl[1] - np.diff(xl) / 50, yl[0] + np.diff(yl) / 50, 
            'FeT-H2O, S0, $\\lambda = ' + f'{f.lam}' +\
            ',$ $' + f'T = {temp}' + '$, unweighed, iter: '+ f'{niter}',
            ha='right', weight='bold')

    ax.legend(loc='best')
    ax2 = ax.twinx() 
    ax2.plot(f.target, f.weights, 'o', alpha=0.3, label='Weights')
    ax2.legend(loc='right')
    ax2.set_ylabel('Weight Values')
    fig.savefig(oname + '.pdf')
    plt.close()
