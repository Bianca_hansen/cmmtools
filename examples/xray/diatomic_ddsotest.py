import numpy as np
from ase import Atoms, units
from ase.constraints import Hookean
from cmm.xray.debye import Debye
from cmm.xray.xpot import DebyeCalc
from cmm.tools.helpercalcs import DoubleCalc, SimpleBond
from ase.md.verlet import VelocityVerlet
from ase.md.nvtberendsen import NVTBerendsen
from ase.calculators.tip3p import TIP3P

atoms = Atoms('CC', positions=np.array([[-1, 0, 0], [1, 0, 0]]))

k = 500 * units.kJ / units.mol
atoms.calc = SimpleBond([[0, 1]], [k], [1.5])

#dyn = NVTBerendsen(atoms, timestep=0.2 * units.fs, temperature=0.00001 * units.kB, taut=50 * units.fs,
#                   trajectory='correct.traj', logfile='correct.log')

#dyn.run(10000)

correct_atoms = Atoms('CC', positions=np.array([[-.75, 0, 0], [.75, 0, 0]]))
db = Debye()
s_correct = db.debye(correct_atoms)


scat_atoms =  Atoms('CC', positions=np.array([[-1, 0, 0], [1, 0, 0]]))

### Only gave the oscillation (for tuned k), but not the damping. Not quiiite sure why?
scat_atoms.calc = DebyeCalc(db, s_correct, k=5e-5)

dyn = VelocityVerlet(scat_atoms, timestep=0.2 * units.fs,
                     trajectory='debye_driven.traj', logfile='debye_driven.log')

#Get "uncorrected" trajectory, e.g. bad spring constant: 
scat_atoms.calc =  SimpleBond([[0, 1]], [1.5 * k], [1.5])

dyn = NVTBerendsen(scat_atoms, timestep=0.2 * units.fs, temperature=0.00001 * units.kB, taut=50 * units.fs,
                   trajectory='uncorrected.traj', logfile='uncorrected.log')

dyn.run(10000)

scat_atoms.calc = DoubleCalc(DebyeCalc(Debye(), s_correct, k=1e-2), 
                             SimpleBond([[0, 1]], [1.5 * k], [1.5]))

dyn = NVTBerendsen(scat_atoms, timestep=0.2 * units.fs, temperature=0.00001 * units.kB, taut=50 * units.fs,
                   trajectory='double_calc.traj', logfile='double_calc.log')

def get_dist(atoms=scat_atoms):
    print(f'{atoms.get_distance(0, 1):2.4f}')

dyn.attach(get_dist)
dyn.run(10000)
