import numpy as np
from ase.io import read
from pathlib import Path
from cmm.xray.noise_estimator import NoiseEstimator
import matplotlib.pyplot as plt

''' Example of using NoiseEstimator based on
    https://doi.org/10.1107/S1600577520002738

    We will estimate the noise on the dS(Q) signal
    from TlPtPOP, a molecule CMMTools has some xyzs
    of anyway.
'''

### Read in atoms objects
here = Path(__file__).parent
atoms_on = read(here / '../../data/md/tlptpop/es_run1_last_step.xyz')
atoms_off = read(here / '../../data/md/tlptpop/gs_run1_last_step.xyz')

# Reference data from S-cube (https://github.com/Jkim9486/Scube); PAL-XFEL
fref = 30.0         # ref. repetition rate (Hz, pulses/sec)
nref = 1.0e+12      # ref. photons/pulse
Dref = 1 / fref     # detector exposure time
Nref = nref * fref * Dref    # photons/sec
scale = 7.3094e+04  # scaling factor from exp. data

# Sample parameters
dens = 779.6  # solvent density, kg/m^3
mw = 0.0842   # solvent molar mass, kg/mol (cyclohexane)
conc = 50     # solute concentration, mM
conc_scale = dens / mw / conc  # scale solute scattering according to concentration
frac_exc = 0.4     # frction of solute molecules that are excited in experiment;
frac_yield = 1.0    # fraction of excited solute molecules that go to the correct ES


# Data collection parameters
t = 2        # Accumulation time, sec
# X-ray pulse parameters
n = 1e12       # photons/pulse for (target) measurements
f = 120.0      # pulse repetition rate (pulses/sec)
D = 1 / f      # detector exposure time
N = n * f * D  # photons/sec
num_curves = round(t / D)  # Number of scattering curves
norm = np.sqrt(N / Nref)


ne = NoiseEstimator(num_curves=num_curves, norm=norm, scale=scale)

noise_diff = ne.calc_noise()
scaled_ds = ne.calculate_scaled_ds(atoms_off, atoms_on,
                                   frac_exc, frac_yield, conc_scale)

# Signal with noise:
ds_noisy = scaled_ds + noise_diff
fig, ax = plt.subplots()
ax.plot(ne.qvec, ds_noisy, 'C3', lw=2, label='Noisy signal')
ax.plot(ne.qvec, scaled_ds, 'C0', lw=1, label='Debye')
ytext = r'$\Delta$S (e.u.)'
xtext = 'q (Å$^{-1}$)'
ax.set_ylabel(ytext)
ax.set_xlabel(xtext)
ax.legend(loc='best')
plt.show()
