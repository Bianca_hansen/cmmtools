import rmsd, glob
import numpy as np
from os.path import isdir
from simtk.unit import kilojoule_per_mole, angstroms
import simtk.unit as u
from cmm.md.cmmsystems import CMMSystem
from cmm.tools.parsers import read_chargefile
from cmm.tools.ase_openmm import sys_as_ase
from parmed.openmm.reporters import RestartReporter
from ase.io import read
from simtk.openmm.openmm import HarmonicAngleForce, HarmonicBondForce 

'''  MePtPOP Solvation Shell sampling run, Kruppa structure (Me= Ag, Pt):
    
     All 3 metal positions from first round of structural x-ray fitting.
     
     The Kruppa structures are -2 and has an extra hydrogen. We do not
     want to reparametrize the entire complex for this small change, so 
     we absorb the extra hydrogen charge (idx 39) into the closest hydrogen
     (idx 28)

     ChelpG charges from ORCA, using the COSMO vdw radii. 

    1. Start from the same good AgPtPOP trajectory as with the Ag runs
    2. Reset the position to the Tl geom opt from the orca run
        The atomic sequence is the same, AFTER removial of extra H.
        Ag serves as Tl.
    3. Set the charges: Total charge sum is now -2
    4. For the Tl runs: Change the 'Ag' LJ parameters to those of Tl 
       from 10.1021/ct500918t: rmin/2: 1.870 Ang, 
                               eps:0.27244486 kcal/mol

    The Kruppa structures are indexed differently, so a resort based on 
    Kabsch aligned structures are needed. 
    The charges are calculated on the Kruppa indexing, so they ALSO 
    need a resort. 

    This is tested in the TlPtPOP_Shells notebook

'''

runs = sorted(glob.glob('../../data/md/tlptpop/Kruppa/??PtPOP_Krupp_??_fitted_trj*'))
for run in runs:
    me = run.split('/')[-1][:2].lower()
    state = run.split('_')[2]

    chargefile = f'../../data/md/tlptpop/Kruppa/{me}ptpop_kruppa_{state.lower()}_charges.dat'
    with open(chargefile, 'r') as f:
        lines = f.readlines()
    versions = [line for line in lines if ': ESP char' in line]

    for ii, vers in enumerate(versions):
        ptval = vers.split(f'Pt')[-1].split('.log')[0]
        meval = vers.split(f'{me.capitalize()}')[-1].split('_')[0]
        print(vers, ptval, meval)

        # New charges:
        charges, elements = read_chargefile(chargefile, f'{me.capitalize()}{meval}_Pt{ptval}')
        charges = charges[:, 1]
        charges[28] += charges[39]  # adding the extra H charge to the closest O.
        ## they LATER need resorting!

        equilname = f'{me}ptpop_{state}_kruppa_vdw_Pt{ptval}_{me.capitalize()}{meval}_equi'
        produname = f'{me}ptpop_{state}_kruppa_vdw_Pt{ptval}_{me.capitalize()}{meval}_prod'
        fpath = f'/work1/asod/openMM/data/KRUPPA_VDW/{me.capitalize()}PtPOP_{state}_Kruppa_vdw_Pt{ptval}_{me.capitalize()}{meval}/'
        if isdir(fpath):
            print(f'{fpath} already exists, skipping..')
            continue

        #### init - already pushed away ions when we start
        sys = CMMSystem(equilname, fpath=fpath)

        sys.system_from_prmtop('../../data/md/agptpop/agptpop_solv_custom.prmtop')

        sys.load_rst7('/zhome/c7/a/69784/openMM/data/AgPtPOP/STARTFROMTHIS/agptpop_nvt_pushaway_Pt1.50_Ag2.25.rst7')


        if me == 'tl':
            # Set the LJ parameter of the 'Ag' atom to the ones of Tl, effectively making the atom Tl
            rminhlf =  1.870 
            sig_tl = rminhlf * 2**(5/6)
            eps_tl = 0.27244486 * u.kilocalorie_per_mole
            ljdict = {'AG':(eps_tl, sig_tl * u.angstroms)}
            sys.adjust_lj([0], ljdict)

        # Set positions to KRUPPA PtPOP
        ns_idx, syms = sys.get_nonsolvent_indices()
        solu_idx = [i for i, sym in enumerate(syms) if sym != 'Na+']
        old_atoms = sys_as_ase(sys, solu_idx)
        old_pos = old_atoms.get_positions()
        kruppa = read(run + '@-1')
        pos_kru = kruppa.get_positions()
        pos_kru += -rmsd.centroid(pos_kru) + rmsd.centroid(old_pos)
        # rotate
        # U = rmsd.kabsch(pos_kru[1:-1], old_pos[1:])  # Kabsch to get rotmat. 1:-1 to ignore Metal and last H
        # The kabsch fails if not solely aligning the Pt atoms... 
        U = rmsd.kabsch(pos_kru[[atom.index for atom in kruppa if (atom.symbol == 'X') or (atom.symbol == 'Pt')]], 
                        old_pos[[atom.index for atom in old_atoms if (atom.symbol == 'X') or (atom.symbol == 'Pt')]])  
        pos_aligned = np.dot(pos_kru, U)

        kruppa.set_positions(pos_aligned)

        ### Resort
        new_pos = kruppa.get_positions()[:-1]
        new_syms = kruppa.get_chemical_symbols()[:-1]  # not the last H
        index_map = [0] # first metal is the same
        for o, oa in enumerate(old_atoms[1:39]):
            old_sym = oa.symbol
            dists = np.linalg.norm(new_pos - oa.position, axis = 1)  # all dists from oa to kruppa
            # find the shortest distance to an atom OF THE SAME ELEMENT   
            idx = np.ma.argmin(np.ma.MaskedArray(dists, [ns != old_sym for ns in new_syms]), fill_value=100)
            index_map.append(idx)
                
            print(f'old atom {o}, {old_sym} is closest to new {idx}, {new_syms[idx]}, distance: {dists[idx]}')

            
        ### Use the index map to update positions in sys AND resort charges
        pos = sys.positions.value_in_unit(angstroms)  # get them to np array first
        pos = np.array([[p.x, p.y, p.z] for p in pos])
        pos[solu_idx] = pos_aligned[index_map]  # update positionss

        # put them back
        sys.update_positions(pos)

        # put them back
        sys.update_positions(pos)

        # resort charges!
        charges = np.array(charges)[index_map]

        # set new charges
        sys.adjust_solute_charges(solu_idx, charges)

        # constrain metal distances
        tlpt = np.linalg.norm(pos[0] - pos[2]) * angstroms
        sys.add_constraint(0, 2, tlpt)
        for i in range(8622):
            c = sys.system.getConstraintParameters(i) 
            if c[0] == 0:
                print(i, c)
                break
        ptpt = np.linalg.norm(pos[1] - pos[2]) * angstroms
        sys.add_constraint(1, 2, ptpt)

        # RESTRAIN ATOMS
        sys.restrain_atoms(ns_idx, 2000)

        # and integrator
        sys.set_integrator(timestep=0.5)  
        #sys.integrator.setConstraintTolerance(1e-12)

        # init sim
        sys.init_simulation(platform='CUDA', double=False)

        # Minimize.. We suddently have a Kruppa metal..
        sys.minimize()

        # MBD
        sys.mbd(temperature=5)

        # reporters
        sys.standard_reporters(step=1000)

        # restart reporter
        restrt = RestartReporter(fpath + f'{equilname}.rst7', 1000) 
        sys.add_reporter(restrt)

        # runnnn
        sys.run(time_in_ps=200)

        ## Production
        sys = CMMSystem(produname, fpath=fpath)
        sys.system_from_prmtop('../../data/md/agptpop/agptpop_solv_custom.prmtop')
        sys.load_rst7(fpath + f'{equilname}.rst7')

        # constrain metal distances
        tlpt = np.linalg.norm(pos[0] - pos[2]) * angstroms
        sys.add_constraint(0, 2, tlpt)
        ptpt = np.linalg.norm(pos[1] - pos[2]) * angstroms
        sys.add_constraint(1, 2, ptpt)

        # RESTRAIN ATOMS
        sys.restrain_atoms(ns_idx, 2000)

        # set new charges
        sys.adjust_solute_charges(solu_idx, charges)

        if me == 'tl':
            # and new LJ params 
            rminhlf =  1.870 
            sig_tl = rminhlf * 2**(5/6)
            eps_tl = 0.27244486 * u.kilocalorie_per_mole
            ljdict = {'AG':(eps_tl, sig_tl * u.angstroms)}
            sys.adjust_lj([0], ljdict)

        # and integrator
        sys.set_integrator(timestep=2.0)  

        # init sim
        sys.init_simulation(platform='CUDA', double=False)


        # reporters
        sys.standard_reporters(step=250)

        # restart reporter
        restrt = RestartReporter(fpath + f'{produname}.rst7', 1000) 
        sys.add_reporter(restrt)

        # runnnn
        try:
            sys.run(time_in_ps=10000)
        except:
            print('Run died, sadsmiley')
